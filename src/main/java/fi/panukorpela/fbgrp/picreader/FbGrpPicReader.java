package fi.panukorpela.fbgrp.picreader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FbGrpPicReader {
    private static final String OUTPUT_DIRECTORY = "output/";
    private static Log log = LogFactory.getLog(FbGrpPicReader.class);
    private static DateTimeFormatter inFormatterYear = new DateTimeFormatterBuilder()
            .appendPattern("d MMMM yyyy 'at' HH:mm")
            .toFormatter(Locale.ENGLISH).withLocale(Locale.UK);
    private static DateTimeFormatter inFormatterNoYear = new DateTimeFormatterBuilder()
            .appendPattern("d MMMM 'at' HH:mm")
            .parseDefaulting(ChronoField.YEAR, 2021)
            .toFormatter(Locale.ENGLISH).withLocale(Locale.UK);
    private static DateTimeFormatter inFormatterYearNoTime = new DateTimeFormatterBuilder()
            .appendPattern("d MMMM yyyy")
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .toFormatter(Locale.ENGLISH).withLocale(Locale.UK);
    private static DateTimeFormatter inFormatterNoYearOrTime = new DateTimeFormatterBuilder()
            .appendPattern("d MMMM")
            .parseDefaulting(ChronoField.YEAR, 2021)
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .toFormatter(Locale.ENGLISH).withLocale(Locale.UK);
    private static DateTimeFormatter outFormatter = DateTimeFormatter.ofPattern(
            "yyyy-MM-dd'_'HH.mm");

    public static void main(String[] args) throws Exception {
        // Credentials
        Properties prop = new Properties();
        File propFile = new File("config.properties");
        if (propFile.exists()) {
            try (InputStream propInputStream = new FileInputStream(propFile)) {
                prop.load(propInputStream);
            }
        } else {
            throw new FileNotFoundException(
                    "property file 'config.properties' not found");
        }
        // Output directory
        File directory = new File(OUTPUT_DIRECTORY +
                prop.getProperty("subdirectory"));
        if (!directory.exists()) {
            directory.mkdir();
        }
        // Web UI handling
        System.setProperty("webdriver.gecko.driver", prop.getProperty("geckodriver"));
        FirefoxOptions options = new FirefoxOptions();
        WebDriver webDriver = new FirefoxDriver(options);
        // Login
        webDriver.navigate().to("https://www.facebook.com/login");
        // Cookies
        (new WebDriverWait(webDriver, Duration.ofSeconds(20)))
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//button[@title = 'Hyväksy kaikki']")));
        WebElement element = webDriver.findElement(
                By.xpath("//button[@title = 'Hyväksy kaikki']"));
        element.click();
        // Actual login
        element = webDriver.findElement(By.cssSelector("#email"));
        element.sendKeys(prop.getProperty("username"));
        element = webDriver.findElement(By.cssSelector("#pass"));
        element.sendKeys(prop.getProperty("password"));
        element = webDriver.findElement(By.cssSelector("#loginbutton"));
        element.click();
        (new WebDriverWait(webDriver, Duration.ofSeconds(20)))
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//h3[contains(text(), 'Create a post')]")));
        // Wrapper for pictures
        webDriver.navigate().to(prop.getProperty("url"));
        (new WebDriverWait(webDriver, Duration.ofSeconds(20)))
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//span[contains(text(), 'Media')]/../../../../div[1]/div[2]")));
        element = webDriver.findElement(By.xpath("//span[contains(text(), 'Media')]/../../../../div[1]/div[2]"));
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        ((JavascriptExecutor) webDriver).executeScript(scrollElementIntoMiddle, element);
        // Handle pictures
        boolean morepictures = false;
        List<WebElement> allViewablePictures = element.findElements(By.xpath("//a[starts-with(@href, '/photo/')]"));
        log.info("allViewablePictures initial size: " + allViewablePictures.size());
        if (allViewablePictures.size() > 0) {
            WebElement pictureThumbnailElement = allViewablePictures.get(0);
            int pictureIndex = 0;
            do {
                ((JavascriptExecutor) webDriver).executeScript(
                        scrollElementIntoMiddle, pictureThumbnailElement);
                log.info("scrolled to middle");
                pictureThumbnailElement.click();
                (new WebDriverWait(webDriver, Duration.ofSeconds(30)))
                        .until(ExpectedConditions.presenceOfElementLocated(
                                By.xpath("//div[@data-name = 'media-viewer-nav-container']/following-sibling::div//img")));
                // Get picture
                WebElement pictureElement = webDriver.findElement(
                        By.xpath("//div[@data-name = 'media-viewer-nav-container']/following-sibling::div//img"));
                String src = pictureElement.getAttribute("src");
                // Date for file name
                (new WebDriverWait(webDriver, Duration.ofSeconds(20)))
                        .until(ExpectedConditions.presenceOfElementLocated(
                                By.xpath(prop.getProperty("filenameXpath") 
                                + " " + prop.getProperty("filenameXpath2")
                                + " | " + 
                                prop.getProperty("filenameXpathOld"))));
                String dateStr = null;
                WebElement dateElement = null;
                try {
                    dateElement = webDriver.findElement(
                            By.xpath(prop.getProperty("filenameXpath") + " " + prop.getProperty("filenameXpath2")));
                    dateStr = dateElement.getAttribute("aria-label");
                } catch (Exception ex) {
                    dateElement = webDriver.findElement(
                            By.xpath(prop.getProperty("filenameXpathOld")));
                    dateStr = dateElement.getText();
                }
                System.out.println(dateStr);
                String authorStr = null;
                try {
                    WebElement authorElement = webDriver.findElement(
                        By.xpath(prop.getProperty("filenameXpath") + "]"));
                    authorStr = authorElement.getAttribute("aria-label");
                } catch (Exception ex) {
                    WebElement authorElement = webDriver.findElement(
                        By.xpath(prop.getProperty("filenameXpathOld2")));
                    authorStr = authorElement.getText();
                }
                String fileName = OUTPUT_DIRECTORY + 
                        prop.getProperty("subdirectory") +
                        "/" + parseDate(dateStr) +
                        "_" + authorStr.replace(" ", "_") +
                        "_" + fileNameFromUrl(src);
                // Store picture
                InputStream in = new URL(src).openStream();
                Files.copy(in, Paths.get(fileName), StandardCopyOption.REPLACE_EXISTING);
                in.close();
                // Picture text
                WebElement descriptionTextElement = null;
                try {
                    descriptionTextElement = dateElement.findElement(
                            By.xpath("../../../../../../../../following-sibling::div/span[1]"));
                } catch (Exception ex) {
                    System.out.println("description not found");
                }
                if (descriptionTextElement != null) {
                    Files.writeString(Paths.get(fileName + ".html"), 
                            descriptionTextElement.getAttribute("outerHTML"),
                            StandardOpenOption.CREATE,
                            StandardOpenOption.TRUNCATE_EXISTING);
                }
                pictureIndex++;
                log.info("next index: " + pictureIndex);
                WebElement closeButton = webDriver.findElement(By.xpath("//div[@aria-label = 'Close']"));
                closeButton.click();
                log.info("closed");
                // Next picture
                Thread.sleep(2000);
                (new WebDriverWait(webDriver, Duration.ofSeconds(20)))
                        .until(ExpectedConditions.presenceOfElementLocated(
                                By.xpath("//span[contains(text(), 'Media')]/../../../../div[1]/div[2]")));
                element = webDriver.findElement(By.xpath("//span[contains(text(), 'Media')]/../../../../div[1]/div[2]"));
                allViewablePictures = element.findElements(By.xpath("//a[starts-with(@href, '/photo/')]"));
                log.info("allViewablePictures size: " + allViewablePictures.size());
                if (allViewablePictures.size() > pictureIndex) {
                    pictureThumbnailElement = allViewablePictures.get(pictureIndex);
                    morepictures = true;
                } else {
                    morepictures = false;
                }
            } while (morepictures);
        }
        webDriver.quit();
        System.exit(0);   // Release all Selenium threads 
    }
    
    static String parseDate(String input) {
        // First without 
        LocalDateTime dateTime = null;
        try {
            dateTime = LocalDateTime.parse(input, inFormatterNoYear);
        } catch (Exception ex) { }
        if (dateTime == null) {
            try {
                dateTime = LocalDateTime.parse(input, inFormatterNoYearOrTime);
            } catch (Exception ex) { }
        }
        if (dateTime == null) {
            try {
                dateTime = LocalDateTime.parse(input, inFormatterYearNoTime);
            } catch (Exception ex) { }
        }
        if (dateTime == null) {
            try {
                dateTime = LocalDateTime.parse(input, inFormatterYear);
            } catch (Exception ex) { }
        }
        return outFormatter.format(dateTime);
    }
    
    static String fileNameFromUrl(String url) {
        url = url.substring(url.lastIndexOf("/") + 1);
        url = url.substring(0, url.indexOf("?"));
        return url;
    }
}
