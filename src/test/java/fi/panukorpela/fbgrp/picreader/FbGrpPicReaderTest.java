package fi.panukorpela.fbgrp.picreader;

import org.junit.jupiter.api.Test;

public class FbGrpPicReaderTest {

    @Test
    public void parseDate() {
        String formattedDate = FbGrpPicReader.parseDate("26 June at 17:45");
        System.out.println(formattedDate);
        formattedDate = FbGrpPicReader.parseDate("25 June");
        System.out.println(formattedDate);
        formattedDate = FbGrpPicReader.parseDate("25 March");
        System.out.println(formattedDate);
        formattedDate = FbGrpPicReader.parseDate("10 December 2019");
        System.out.println(formattedDate);
        formattedDate = FbGrpPicReader.parseDate("10 December 2019 at 12:34");
        System.out.println(formattedDate);
    }
    
    @Test
    public void fileNameFromUrl() {
        String originalFileName = FbGrpPicReader.fileNameFromUrl(
                "https://scontent-hel2-1.xx.fbcdn.net/v/t1.0-9/p843x403/106204514_1176042546068544_4570607869436122335_o.jpg?_nc_cat=105&amp;_nc_sid=b9115d&amp;_nc_ohc=BSdM8oqva3kAX8Om1qo&amp;_nc_ht=scontent-hel2-1.xx&amp;_nc_tp=6&amp;oh=510ff00cf85cc17e5fe7ef92f7af3bd5&amp;oe=5F405A1C");
        System.out.println(originalFileName);
    }
}
